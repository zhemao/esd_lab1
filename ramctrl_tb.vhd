library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ramctrl_tb is
end ramctrl_tb;

architecture test of ramctrl_tb is
	signal clk : std_logic;
	signal inc, dec : std_logic;
	signal write_en : std_logic;
	signal write_val : unsigned(7 downto 0);
	signal addr : unsigned(3 downto 0);
	signal read_val : unsigned(7 downto 0);
	component ramctrl
		port(clk    : in std_logic;
			  write_en : in std_logic;
			  write_val : in unsigned(7 downto 0);
			  addr   : in unsigned(3 downto 0);
			  read_val : out unsigned(7 downto 0));
	end component;
	component incdec
		port(
			oldval : in unsigned(7 downto 0);
			inc, dec : in std_logic;
			change : out std_logic;
			newval : out unsigned(7 downto 0));
	end component;
begin
	RC : ramctrl port map (
		clk => clk,
		write_en => write_en,
		write_val => write_val,
		addr => addr,
		read_val => read_val
	);
	ID : incdec port map (
		inc => inc,
		dec => dec,
		oldval => read_val,
		newval => write_val,
		change => write_en
	);
	process
	begin
		clk <= '1';
		wait for 10 ns;
		clk <= '0';
		wait for 10 ns;
	end process;
	
	process
	begin
		addr <= x"0";
		inc <= '0';
		dec <= '0';
		wait for 20 ns;
		wait for 15 ns;
		inc <= '1';
		wait for 10 ns;
		inc <= '0';
		wait for 10 ns;
		addr <= x"2";
		wait;
	end process;
end test;