library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lab1_tb is
end lab1_tb;

architecture test of lab1_tb is
	signal clk : std_logic;
	signal key : std_logic_vector(3 downto 0);
	signal hex2, hex1, hex0 : std_logic_vector(6 downto 0);
	signal valdebug : unsigned(7 downto 0);
	signal addrdebug : unsigned(3 downto 0);
	component lab1
		port (
			clk : in std_logic;
			key : in std_logic_vector(3 downto 0);
			valdebug : out unsigned(7 downto 0);
			addrdebug : out unsigned(3 downto 0);
			hex2, hex1, hex0 : out std_logic_vector(6 downto 0));
	end component;
begin
	L1 : lab1 port map (
		clk => clk,
		hex2 => hex2,
		hex1 => hex1,
		hex0 => hex0,
		key => key,
		valdebug => valdebug,
		addrdebug => addrdebug
	);
	process
	begin
		clk <= '1';
		wait for 10 ns;
		clk <= '0';
		wait for 10 ns;
	end process;
	
	process
	begin
		key <= "1111";
		wait for 5 ns;
		key <= "0000";
		wait for 1 ms;
		key(1) <= '1';
		wait for 1 ms;
		key(1) <= '0';
		wait for 1 ms;
		key(1) <= '1';
		wait for 5 ms;
		key(1) <= '0';
		wait for 10 ms;
		key(3) <= '1';
		wait for 5 ms;
		key(3) <= '0';
		wait;
	end process;
end test;