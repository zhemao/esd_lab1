library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ramctrl is
    port(clk       : in std_logic;
         write_en  : in std_logic;
         write_val : in unsigned(7 downto 0);
         addr      : in unsigned(3 downto 0);
         read_val  : out unsigned(7 downto 0));
end ramctrl;

architecture rtl of ramctrl is
    type ram_type is array(0 to 15) of unsigned(7 downto 0);
    signal RAM : ram_type := 
        ("00000000", "00000000", "00000000", "00000000", 
         "00000000", "00000000", "00000000", "00000000", 
         "00000000", "00000000", "00000000", "00000000", 
         "00000000", "00000000", "00000000", "00000000");
begin
    read_val <= RAM(to_integer(addr));
    
    process (clk)
    begin
        if rising_edge(clk) and write_en = '1' then
            RAM(to_integer(addr)) <= write_val;
        end if;
    end process;
end rtl;