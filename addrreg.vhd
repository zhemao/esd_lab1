library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity addrreg is
    port(clk       : in std_logic;
         inc       : in std_logic;
         dec       : in std_logic;
         addrout   : out unsigned(3 downto 0));
end addrreg;

architecture rtl of addrreg is
    signal addr : unsigned(3 downto 0) := "0000";
    signal input : std_logic_vector(1 downto 0);
begin
    addrout <= addr;
    input <= inc & dec;
    
    process (clk)
    begin
        if rising_edge(clk) then
            case input is
                when "10" =>
                    addr <= addr + 1;
                when "01" =>
                    addr <= addr - 1;
                when others =>
                    addr <= addr;
            end case;
        end if;
    end process;
end rtl;