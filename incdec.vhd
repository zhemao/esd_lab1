library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity incdec is
    port(oldval : in unsigned(7 downto 0);
         inc, dec : in std_logic;
         change : out std_logic;
         newval : out unsigned(7 downto 0));
end incdec;

architecture rtl of incdec is
    signal input : std_logic_vector(1 downto 0);
begin
    input <= inc & dec;
    with input select
        newval <= oldval + 1 when "10",
                     oldval - 1 when "01",
                     oldval when others;
    change <= inc xor dec;
end rtl;