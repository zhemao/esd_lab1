library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lab1 is
    port(key : in std_logic_vector(3 downto 0);
         clk : in std_logic;
         valdebug : out unsigned(7 downto 0);
         addrdebug : out unsigned(3 downto 0);
         hex0, hex1, hex2 : out std_logic_vector(6 downto 0));
end lab1;

architecture rtl of lab1 is
    signal incval, decval, incaddr, decaddr : std_logic;
    signal addr : unsigned(3 downto 0);
    signal ramwrite : unsigned(7 downto 0);
    signal write_en : std_logic;
    signal ramval : unsigned(7 downto 0);
    signal ramvalhigh, ramvallow : unsigned(3 downto 0);
    component debouncer
        port(button : in std_logic;
             clk    : in std_logic;
             output : out std_logic);
    end component;
    component ramctrl
        port(clk       : in std_logic;
             write_en  : in std_logic;
             write_val : in unsigned(7 downto 0);
             addr      : in unsigned(3 downto 0);
             read_val  : out unsigned(7 downto 0));
    end component;
    component incdec
        port(oldval : in unsigned(7 downto 0);
             inc, dec : in std_logic;
             change : out std_logic;
             newval : out unsigned(7 downto 0));
    end component;
    component addrreg
        port(clk     : in std_logic;
             inc     : in std_logic;
             dec     : in std_logic;
             addrout : out unsigned(3 downto 0));
    end component;
    component sevenseg
        port(number  : in unsigned(3 downto 0);
             display : out std_logic_vector(6 downto 0));
    end component;
begin
    ramvalhigh <= ramval(7 downto 4);
    ramvallow <= ramval(3 downto 0);

    IV : debouncer port map (
        button => key(3),
        output => incval,
        clk => clk
    );
    
    DV : debouncer port map (
        button => key(2),
        output => decval,
        clk => clk
    );
    
    IA : debouncer port map (
        button => key(1),
        output => incaddr,
        clk => clk
    );
    
    DA : debouncer port map (
        button => key(0),
        output => decaddr,
        clk => clk
    );
    
    AR : addrreg port map (
        inc => incaddr,
        dec => decaddr,
        addrout => addr,
        clk => clk
    );
    
    ID : incdec port map (
        inc => incval,
        dec => decval,
        oldval => ramval,
        newval => ramwrite,
        change => write_en
    );
    
    RC : ramctrl port map (
        write_val => ramwrite,
        read_val => ramval,
        write_en => write_en,
        clk => clk,
        addr => addr
    );
    
    SS0 : sevenseg port map (
        number => ramvallow,
        display => hex0
    );
    
    SS1 : sevenseg port map (
        number => ramvalhigh,
        display => hex1
    );
    
    SS2 : sevenseg port map (
        number => addr,
        display => hex2
    );
    
    valdebug <= ramval;
    addrdebug <= addr;
end rtl;