library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity debouncer is
    port(button  : in std_logic;
         clk     : in std_logic;
         output  : out std_logic);
end debouncer;

architecture rtl of debouncer is
    type state_type is (
        READY,
        TRIGGERED,
        SLEEPING,
        HOLDING
    );
    signal state : state_type := READY;
    signal counter : unsigned(19 downto 0) := x"00000";
begin
    output <= '1' when state = TRIGGERED else '0';
    process (clk)
    begin
        if rising_edge(clk) then
            case state is
                when READY =>
                    if button = '1' then
                        state <= TRIGGERED;
                    else
                        state <= READY;
                    end if;
                when TRIGGERED =>
                    state <= SLEEPING;
                when SLEEPING =>
                    if counter = x"7a120" then -- 500000 * 20 ns = 10 ms
                        counter <= x"00000";
                        state <= HOLDING;
                    else
                        counter <= counter + 1;
                        state <= SLEEPING;
                    end if;
                when HOLDING =>
                    if button = '1' then
                        state <= HOLDING;
                    else
                        state <= READY;
                    end if;
            end case;
        end if;
    end process;
end rtl;